import { writable } from 'svelte/store';
import ls2 from "localStorage";
import { dev } from '$app/env';

let ls;
if (typeof localStorage !== `undefined`) { 
	ls = localStorage 
}else{
	ls=ls2;
};

export const user = writable(null);

class User {
	_USER_KEY = "USER";
	/*
	{
		"id": 4,
		"username": "test",
		"token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJ0ZXN0Iiwicm9sZXMiOiJBZG1pbiIsIkRhdGUiOiIxNC8xLzIwMjIgMTQ6Mzk6MTIiLCJqdGkiOiI4MGY4YjVjZC00Y2JkLTQxOTMtOTk0MC1hZGQ1ZTJiZjdhZWIiLCJleHAiOjE2NDIxNDk1NTIsImlzcyI6ImZyZWVjb2Rlc3BvdC5jb20iLCJhdWQiOiJmcmVlY29kZXNwb3QuY29tIn0.4NOJxK2SX7iCCXC8xwri7mFjHSsqIc6plaWuKH0V1mk"
	}
	*/

	saveUser(u) {
		ls.setItem(this._USER_KEY, JSON.stringify(u));
		user.update(() => u);
	}

	getUser() {
		return JSON.parse(ls.getItem(this._USER_KEY));
	}

	removeUser() {
		ls.removeItem(this._USER_KEY);
		user.update(u => null);
	}
}

var userStorage = new User();

user.update(() => {
	const u = userStorage.getUser();
	if (dev) {
		console.log('getUser', u)
	}
	return u;
});

export default userStorage;
