import { dev } from "$app/env";


const DOMAIN = dev ? 'http://localhost:7238' : 'http://sinoas01.sino-land.com:1017';

const API_PATH = {
	updateEffDateOfArTenancy: '/ArTenancyIncomeInformation/effect-date',
	reg: '/users/register',
	auth: '/users/authenticate',
	updateChargesWatchmenFeeAndConveyance: '/UpdateChargesWatchmenFeeAndConveyance',
	addDepositByCashOrCheque: '/UpdateDistraintSystemDeposit/AddDepositByCashOrCheque',
	addressName: '/AddressBook/names',
	projectName: '/Project/names',
	updateReimbursementOfBudgetManagementFee: '/ReimbursementOfBudgetManagementFee',
	writeLog: '/Log/WriteLog',
};

const NAV_MENU = [
	{
		title: "Update eff date of AR Tenancy Income Information",
		href: "updateEffDateOfArTenancy",
	},
	{
		title: "Update charges (watchmen's fee / conveyance etc.)",
		href: "updateChargesWatchmenFeeAndConveyance",
	},
	{
		title: "Add deposit (by cash / by cheque)",
		href: "addDepositByCashOrByCheque",
	},
	{
		title: "Change the Reimbursement of Budget Management Fee",
		href: "updateReimbursementOfBudgetManagementFee",
	},
	{
		title: "Write log",
		href: "log",
	},
]

export { DOMAIN, API_PATH, NAV_MENU };
