import moment from "moment"


const toCSharpDate = (mmddyyyy) => moment(mmddyyyy, 'MM/DD/YYYY').utc(true).toISOString();



export { toCSharpDate }