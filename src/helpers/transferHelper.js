import { DOMAIN } from "./config";
import queryString from 'query-string';

import {user} from '../components/userStorage';

let _user = {};
user.subscribe((v)=>_user=v);

const post = (url = '', data = {} ) => {
    const headers ={
        'Content-Type': 'application/json',
        // 'Access-Control-Allow-Origin': '*'
    }
    if (_user && _user.token){
        headers['Authorization'] = `BEARER ${_user.token}`;
    }

	return fetch(`${DOMAIN}${url}`,
		{
			body: JSON.stringify(data),
            method: 'POST',
            headers,
		}
	);
}

const get = (url = '', data = {}) => {
    const headers ={
    }
    if (_user && _user.token){
        headers['Authorization'] = `BEARER ${_user.token}`;
    }

	return fetch(`${DOMAIN}${url}?${queryString.stringify(data)}`,
		{
            method: 'GET',
            headers,
		}
	);
}


export { post, get }
