import adapter from '@sveltejs/adapter-static';
import { optimizeImports, optimizeCss, icons } from "carbon-preprocess-svelte";

/** @type {import('@sveltejs/kit').Config} */
const config = {
	kit: {
        adapter: adapter({
            fallback: 'index.html'
        }),

		// hydrate the <div id="svelte"> element in src/app.html
		target: '#svelte',
		vite: {
			plugins: [
				process.env.NODE_ENV === 'production' && optimizeCss()
			],
			define:{
				global:{}
			}
		},
		router:false,
        prerender:{
            enabled: false,
        },
	},
	preprocess: [optimizeImports(), icons()],
};



export default config;
